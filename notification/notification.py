# -*- coding: UTF-8 -*-
"""
Python library to send desktop notifications on linux using the notify-send command.

Example call:
    # Create new notification with default values
    notification = Notification()
    # Set urgency level (default: Urgency.NORMAL)
    notification.urgency = Urgency.NORMAL
    # Set time in milliseconds that the notification will be displayed (default: 3000)
    notification.expire_time = 3000
    # Set the notification title that is unique for this application (default: 'Notification')
    notification.app_name = 'Notification'
    # Set the notification icon (default: None)
    notification.icon = None
    # Set the notification category (default: Category.IM)
    notification.category = Category.IM
    # Set the notification title
    notification.summary = 'Super Important Notification'
    # Set the notification body/message
    notification.body = 'Example Notification<br/>This is just an example notification'
    # Send the notification
    notification.send()
"""

import os
import subprocess
from typing import Optional, Union, FrozenSet
from pathlib import Path
from notification import Urgency, Category


class Notification:
    """
    Notification class which allows to set default values and
    send the notification to the desktop
    """

    def __init__(
            self,
            urgency: Urgency = Urgency.NORMAL,
            expire_time: int = 3000,
            app_name: str = 'Notification',
            icon: Optional[str] = None,
            category: Union[str, Category] = Category.IM,
            summary: Optional[str] = None,
            body: Optional[str] = None
    ) -> None:
        if not self.is_notify_available():
            raise ValueError(
                'Could not find the notify-send command. Maybe you need to install "libnotify".'
            )
        # Set default values
        self._urgency = urgency
        self._expire_time = expire_time
        self._app_name = app_name
        self._icon = icon
        self._category = category
        self._summary = summary
        self._body = body

    @staticmethod
    def is_notify_available() -> bool:
        """ Return True if the 'notify-send' command is available, False otherwise """
        try:
            subprocess.check_call(
                ['notify-send', '--version'],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE
            )
        except subprocess.CalledProcessError:
            return False
        return True

    @property
    def urgency_levels(self) -> FrozenSet[Urgency]:
        """ Return a list of all available urgency levels """
        return frozenset(urgency for urgency in Urgency)

    @property
    def stock_icons(self) -> FrozenSet[str]:
        """ Get all icons under /usr/share/icons/*/32x32/**/*.png """
        files = set()
        source_dir = os.path.join(os.sep, 'usr', 'share', 'icons')
        for file_path in Path(source_dir).glob('**/*'):
            if os.path.isfile(str(file_path)):
                if '32x32' in str(file_path):
                    if str(file_path).endswith('.png'):
                        file_name = os.path.basename(str(file_path))
                        file_name = os.path.splitext(file_name)[0]
                        files.add(file_name)
        return frozenset(files)

    @property
    def category_types(self) -> FrozenSet[Category]:
        """ Return a list of all predefined categories """
        return frozenset(category for category in Category)

    @property
    def urgency(self) -> Urgency:
        """ Return default urgency level """
        return self._urgency

    @urgency.setter
    def urgency(
            self,
            value: Urgency
    ) -> None:
        """ Set default urgency level """
        self._urgency = value

    @property
    def expire_time(self) -> int:
        """ Return default expire time """
        return self._expire_time

    @expire_time.setter
    def expire_time(
            self,
            value: int
    ) -> None:
        """ Set default expire time """
        if value <= 0:
            raise ValueError(f'You need to set a time greater or equal to 1: {value}')
        self._expire_time = value

    @property
    def app_name(self) -> str:
        """ Return default application name """
        return self._app_name

    @app_name.setter
    def app_name(
            self,
            value: str
    ) -> None:
        """ Set default application name """
        self._app_name = value

    @property
    def icon(self) -> Optional[str]:
        """ Return default icon or None """
        return self._icon

    @icon.setter
    def icon(
            self,
            value: Optional[str]
    ) -> None:
        """ Set default icon or None """
        if value and value not in self.stock_icons and not os.path.isfile(value):
            raise ValueError(
                f'You need to provide either a valid stock icon or a path to an existing image: \
                "{value}"'
            )
        self._icon = value

    @property
    def category(self) -> Union[str, Category]:
        """ Return default category """
        return self._category

    @category.setter
    def category(
            self,
            value: Union[str, Category]
    ) -> None:
        """ Set default category """
        self._category = value

    @property
    def summary(self) -> Optional[str]:
        """ Return summary/title """
        return self._summary

    @summary.setter
    def summary(
            self,
            value: str
    ) -> None:
        """ Set summary """
        self._summary = value

    @property
    def body(self) -> Optional[str]:
        """ Return message """
        return self._body

    @body.setter
    def body(
            self,
            value: str
    ) -> None:
        """ Set message """
        self._body = value

    def send(self) -> None:
        """ Send a desktop notification using notify-send """
        # Check if mandatory fields were set
        if not self.summary:
            raise ValueError('No notification summary set!')
        if not self.body:
            raise ValueError('No notification body set!')

        # Generate command to execute
        cmd = [
            'notify-send',
            f'--urgency={self.urgency.name.lower()}',
            f'--expire-time={self.expire_time}',
            f'--app-name={self.app_name}'
        ]
        if self.icon:
            cmd.append(f'--icon={self.icon}')
        cmd.append(
            '--category={}'.format(
                self.category.value if isinstance(self.category, Category) else self.category
            )
        )
        cmd.append(self.summary)
        cmd.append(self.body)

        # Send notification
        subprocess.call(cmd)
