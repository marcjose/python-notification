#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
""" Main entry points """

import argparse
from typing import Tuple
from notification import Notification, Urgency, Category


def parse_args() -> Tuple[argparse.ArgumentParser, argparse.Namespace]:
    """ Parse command line arguments """
    parser = argparse.ArgumentParser("Console command to send notification")
    sub_parsers = parser.add_subparsers(help="Sending notification", dest="command")

    # Send Notification
    send_parser = sub_parsers.add_parser('send')
    send_parser.add_argument(
        '--urgency',
        '-u',
        type=str,
        default='normal',
        choices=['low', 'normal', 'critical'],
        help='Urgency level'
    )
    send_parser.add_argument(
        '--expire_time',
        '-e',
        type=int,
        default=3000,
        help='Time to show the notification in milliseconds'
    )
    send_parser.add_argument(
        '--app_name',
        '-a',
        type=str,
        default='Notification',
        help='Notification sender name'
    )
    send_parser.add_argument(
        '--icon',
        '-i',
        type=str,
        default=None,
        help='Stock icon or path to icon file'
    )
    send_parser.add_argument(
        '--category',
        '-c',
        type=str,
        default='im',
        help='Category type'
    )
    send_parser.add_argument(
        '--summary',
        '-s',
        type=str,
        required=True,
        help='Summary / Notification title'
    )
    send_parser.add_argument(
        '--body',
        '-b',
        type=str,
        required=True,
        help='Notification message'
    )
    return (parser, parser.parse_args())

def send_notification(args: argparse.Namespace) -> None:
    """ Send a notification """
    try:
        category = Category.from_str(args.category)
    except ValueError:
        category = args.category

    Notification(
        urgency=Urgency.from_str(args.urgency),
        expire_time=args.expire_time,
        app_name=args.app_name,
        icon=args.icon,
        category=category,
        summary=args.summary,
        body=args.body
    ).send()

def main() -> None:
    """ Main routine, parse arguments and call corresponding functions """
    parser, args = parse_args()

    if args.command == 'send':
        send_notification(args=args)
    else:
        parser.print_help()

if __name__ == '__main__':
    main()
