#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
""" Pytest Suite """

import unittest
from notification import Notification, Category


class CategoryTestCase(unittest.TestCase):
    """
    Test if category types can be correctly set and parsed.
    """

    def test_category_levels(self) -> None:
        """ Test setting notification category types to all possible values. """
        notification = Notification()
        for category_level in notification.category_types:
            notification.category = category_level
            self.assertEqual(notification.category, category_level)

    def test_custom_category(self) -> None:
        """ Test if a custom string can be used as well. """
        custom_category = 'custom.category'
        notification = Notification()
        notification.category = custom_category
        self.assertEqual(custom_category, notification.category)

    def test_converting_str(self) -> None:
        """ Test converting each enum value to its enum. """
        notification = Notification()
        for category_level in notification.category_types:
            self.assertEqual(category_level, Category.from_str(category_level.value))

    def test_converting_invalid_str(self) -> None:
        """ Test converting each enum value to its enum. """
        with self.assertRaises(ValueError):
            Category.from_str('invalid')
