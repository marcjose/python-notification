#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
""" Pytest Suite """

import unittest
import mock
from notification import Notification


class RequirementsTestCase(unittest.TestCase):
    """
    Test if the requirements are met.
    """

    def test_notify_send(self) -> None:
        """ Test if notify-send is available. """
        self.assertTrue(Notification.is_notify_available())

    @mock.patch('notification.Notification.is_notify_available')
    def test_missing_notify_send_init(self, is_notify_available_mock: mock.Mock) -> None:
        """ Test assuming notify-send is *not* available. """
        is_notify_available_mock.return_value = False
        with self.assertRaises(ValueError):
            Notification()
