#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
""" Pytest Suite """

import sys
import unittest
from notification import Notification, Urgency


class NotificationTestCase(unittest.TestCase):
    """
    Test Notification functionality eg setting parameters and sending notification.
    """

    def test_send_empty_summary(self) -> None:
        """ Test sending notification with empty summary. """
        notification = Notification()
        notification.summary = 'Test'
        with self.assertRaises(ValueError):
            notification.send()

    def test_send_empty_body(self) -> None:
        """ Test sending notification with empty body. """
        notification = Notification()
        notification.body = 'Test'
        with self.assertRaises(ValueError):
            notification.send()

    def test_send_empty(self) -> None:
        """ Test sending notification with empty summary/body. """
        notification = Notification()
        with self.assertRaises(ValueError):
            notification.send()

    def test_send_default(self) -> None:
        """ Test sending notification with empty summary/body. """
        notification = Notification()
        notification.expire_time = 1
        notification.summary = 'Test'
        notification.body = 'Test'
        notification.send()

    def test_send(self) -> None:
        """ Test sending notification with all fields set and or changed. """
        notification = Notification()
        notification.urgency = Urgency.LOW
        notification.expire_time = 1
        notification.app_name = 'Test Notification'
        notification.icon = next(iter(notification.stock_icons))
        notification.category = 'my.custom.category'
        notification.summary = 'Test'
        notification.body = 'Test'
        notification.send()

    def test_get_stock_icons(self) -> None:
        """ Test if there are any stock icons available. """
        notification = Notification()
        self.assertTrue(notification.stock_icons)

    def test_set_time(self) -> None:
        """ Test setting time to valid value. """
        notification = Notification()
        expire_time = 1337
        notification.expire_time = expire_time
        self.assertEqual(notification.expire_time, expire_time)

    def test_set_zero_time(self) -> None:
        """ Test setting time to zero. """
        notification = Notification()
        with self.assertRaises(ValueError):
            notification.expire_time = 0

    def test_set_invalid_time(self) -> None:
        """ Test setting negative time. """
        notification = Notification()
        with self.assertRaises(ValueError):
            notification.expire_time = -sys.maxsize

    def test_check_app_name(self) -> None:
        """ Test setting app_name. """
        notification = Notification()
        app_name = 'Test Name'
        notification.app_name = app_name
        self.assertEqual(notification.app_name, app_name)

    def test_set_stock_icon(self) -> None:
        """ Test setting icon to stock icon. """
        notification = Notification()
        for icon in notification.stock_icons:
            notification.icon = icon
            self.assertEqual(notification.icon, icon)
            break

    def test_set_invalid_icon(self) -> None:
        """ Test setting icon to an invalid one. """
        notification = Notification()
        icon = 'gjbrotit8459tvm45ore'
        with self.assertRaises(ValueError):
            notification.icon = icon
